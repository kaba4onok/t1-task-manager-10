package ru.t1.rleonov.tm.api;

public interface ICommandController {

    void showAbout();

    void showVersion();

    void showInfo();

    void showErrorArgument();

    void showErrorCommand();

    void showHelp();

    void showArguments();

    void showCommands();

}
