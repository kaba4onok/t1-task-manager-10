package ru.t1.rleonov.tm.api;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void showProjects();

}
