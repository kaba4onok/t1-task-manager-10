package ru.t1.rleonov.tm.api;

import ru.t1.rleonov.tm.model.Project;
import java.util.List;

public interface IProjectRepository {

    Project add(Project project);

    List<Project> findAll();

    void clear();
}
