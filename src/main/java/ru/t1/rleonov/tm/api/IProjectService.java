package ru.t1.rleonov.tm.api;

import ru.t1.rleonov.tm.model.Project;
import java.util.List;

public interface IProjectService {

    Project add(Project project);

    Project create(String name);

    Project create(String name, String description);

    List<Project> findAll();

    void clear();

}
