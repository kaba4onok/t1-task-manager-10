package ru.t1.rleonov.tm.api;

import ru.t1.rleonov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    Task add(Task task);

    Task create(String name);

    Task create(String name, String description);

    List<Task> findAll();

    void clear();

}
