package ru.t1.rleonov.tm.repository;

import ru.t1.rleonov.tm.api.ICommandRepository;
import ru.t1.rleonov.tm.constant.TerminalArguments;
import ru.t1.rleonov.tm.constant.TerminalCommands;
import ru.t1.rleonov.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private final static Command ABOUT = new Command(
            TerminalCommands.ABOUT, TerminalArguments.ABOUT,
            "Show application info."
    );

    private final static Command INFO = new Command(
            TerminalCommands.INFO, TerminalArguments.INFO,
            "Show system info."
    );

    private final static Command VERSION = new Command(
            TerminalCommands.VERSION, TerminalArguments.VERSION,
            "Show application version."
    );

    private final static Command EXIT = new Command(
            TerminalCommands.EXIT, null,
            "Exit application."
    );

    private final static Command HELP = new Command(
            TerminalCommands.HELP, TerminalArguments.HELP,
            "Show application commands."
    );

    private final static Command COMMANDS = new Command(
            TerminalCommands.COMMANDS, TerminalArguments.COMMANDS,
            "Show application commands."
    );

    private final static Command ARGUMENTS = new Command(
            TerminalCommands.ARGUMENTS, TerminalArguments.ARGUMENTS,
            "Show application commands."
    );

    private final static Command PROJECT_LIST = new Command(
            TerminalCommands.PROJECT_LIST, null,
            "Show project list."
    );

    private final static Command PROJECT_CREATE = new Command(
            TerminalCommands.PROJECT_CREATE, null,
            "Create new project."
    );

    private final static Command PROJECT_CLEAR = new Command(
            TerminalCommands.PROJECT_CLEAR, null,
            "Delete all projects."
    );

    private final static Command TASK_LIST = new Command(
            TerminalCommands.TASK_LIST, null,
            "Show task list."
    );

    private final static Command TASK_CREATE = new Command(
            TerminalCommands.TASK_CREATE, null,
            "Create new task."
    );

    private final static Command TASK_CLEAR = new Command(
            TerminalCommands.TASK_CLEAR, null,
            "Delete all tasks."
    );

    private final static Command[] TERMINAL_COMMANDS = new Command[]{
            COMMANDS, ARGUMENTS, ABOUT, INFO, VERSION, HELP,
            PROJECT_LIST, PROJECT_CREATE, PROJECT_CLEAR,
            TASK_LIST, TASK_CREATE, TASK_CLEAR,
            EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
