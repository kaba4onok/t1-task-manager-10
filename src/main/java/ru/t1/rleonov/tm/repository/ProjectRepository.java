package ru.t1.rleonov.tm.repository;

import ru.t1.rleonov.tm.api.IProjectRepository;
import ru.t1.rleonov.tm.model.Project;
import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository implements IProjectRepository {

    private List<Project> projects = new ArrayList<>();

    @Override
    public Project add(final Project project) {
        projects.add(project);
        return project;
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public void clear() {
        projects.clear();
    }

}
