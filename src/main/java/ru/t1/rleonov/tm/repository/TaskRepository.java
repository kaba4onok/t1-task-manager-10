package ru.t1.rleonov.tm.repository;

import ru.t1.rleonov.tm.api.ITaskRepository;
import ru.t1.rleonov.tm.model.Task;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    private List<Task> tasks = new ArrayList<>();

    @Override
    public Task add(final Task task) {
        tasks.add(task);
        return task;
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

}
